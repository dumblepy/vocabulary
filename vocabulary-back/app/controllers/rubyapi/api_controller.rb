class Rubyapi::ApiController < ActionController::Base
  #Rails4のCSRF対策で「Can't verify CSRF token authenticity」エラー
  #https://qiita.com/chobi9999/items/2b59fdaf3dd8f2ed9268
  #スクレイピング用ライブラリ
  require "open-uri"
  require "nokogiri"
  #csv用ライブラリ
  require 'csv'
  require 'json'
  
  protect_from_forgery :except => [:search, :downloadCSV]

  def test
    render html: '<h1>TEST</h1>'
  end
  
  def search
    puts params
    words = params[:words].split("\n") #配列に変換
    #charset = 'utf-8'

    #レスポンス用配列
    resultArray = {}

    #{
    #  'words': 
    #    [
    #      {
    #        'word': 'apple',
    #        'pron': 'ǽpl',
    #        'meaning': '《植物》リンゴ（の木）◆ユーラシア原産のバラ科リンゴ属（Malus）の落葉樹。◆【学名】Malus pumila'
    #      },
    #      {
    #        'word': 'banana',
    #        'pron': 'bənǽnə',
    #        'meaning': '植物》バナナ'
    #      }
    #    ]
    #  }
    #}

    i = 0
    resultArray['words'] = Array.new(words.length).map{{'key':nil, 'word': nil, 'pron': nil, 'meaning': nil}}

    for word in words do
      resultArray['words'][i][:key] = i
      resultArray['words'][i][:word] = word

      url = 'https://eow.alc.co.jp/search?q=' + word + '&ref=sa'
      html = open(url) do |page|
        #charsetを自動で読み込み、取得
        #charset = page.charset
        #中身を読む
        page.read
      end

      doc = Nokogiri::HTML.parse(html,nil,'utf-8')

      #発音記号--------------------------
      pron = ''
      doc.xpath('//span[@class="pron"]').each do |node|
        pron = node.inner_text.gsub(/、/, '') #末尾句読点削除
      end
      resultArray['words'][i][:pron] = pron

      #意味------------------------------
      meaning = ''
      meaningCount = 0
      first_li = doc.xpath('//div[@id="resultsList"]/ul/li').first
      first_li.xpath('div/ol/li').each do |node|
        if meaningCount < 4 then

          #教育上ふさわしくない意味は除外
          if !node.inner_text.include?('俗') &&
              !node.inner_text.include?('卑') &&
              !node.inner_text.include?('蔑') &&
              !node.inner_text.include?('＝')
          then
            meaningRow = node.inner_text
            
            #【語源】や【学名】削除
            meaningRow = meaningRow.gsub(/(◆.+)+/, '')
            #漢字の読みがな削除
            meaningRow = meaningRow.gsub(/\｛(\p{Hiragana}|\s|（|）|／)+\｝/, '')
            #meaningRow = meaningRow.gsub(/\｛(\p{Hiragana}|\s|\W)+\｝/, '')
            #例文削除
            meaningRow = meaningRow.gsub(/・[a-zA-Z\s]+.+/, '')
            #〔〕内のアルファベット削除
            meaningRow = meaningRow.gsub(/[a-zA-Z]|'/, '-')

            #meaning.push(meaningRow)
            meaning << meaningRow + '、'
            meaningCount += 1
          end
        end
        
      end

      if meaningCount < 1 then
        first_li.xpath('div/ol').each do |node|
          if meaningCount < 4 then

            if !node.inner_text.include?('俗') &&
                !node.inner_text.include?('卑') &&
                !node.inner_text.include?('蔑') &&
                !node.inner_text.include?('＝')
            then
              meaningRow = node.inner_text
              
              #【語源】や【学名】削除
              meaningRow = meaningRow.gsub(/(◆.+)+/, '')
              #漢字の読みがな削除
              #meaningRow = meaningRow.gsub(/\｛(\p{Hiragana}|\s)+\｝/, '')
              meaningRow = meaningRow.gsub(/\｛(\p{Hiragana}|\s|（|）|／)+\｝/, '')
              #例文削除
              meaningRow = meaningRow.gsub(/・[a-zA-Z\s]+.+/, '')
              #〔〕内のアルファベット削除
              meaningRow = meaningRow.gsub(/[a-z]|'/, '-')
  
              #meaning.push(meaningRow)
              meaning << meaningRow + '、'
              meaningCount += 1
            end
          end
        end
      end

      meaning.chop! #末尾の1文字(句読点)削除
      resultArray['words'][i][:meaning] = meaning

      i += 1
    end

    render :json => resultArray
  end

  def downloadCSV
    request = params[:request]
    #bom = %w(EF BB BF).map { |e| e.hex.chr }.join
    bom = "\uFEFF"
    csv_data = CSV.generate(bom) do |csv|
      csv << ['id', '英単語','発音記号', '意味'] #ヘッダー
      request.each do |requestRow|
        csv << [requestRow['key'], requestRow['word'], requestRow['pron'], requestRow['meaning']]
      end
    end
    send_data(csv_data, :filename => 'vocabulary.csv', :type=> 'text/csv; charset=uft-8')
  end

  #http://d.hatena.ne.jp/seinzumtode/20150624/1435115066
  def downloadCSVMac
    request = params[:request]
    bom = "\xFF\xFE".force_encoding("UTF-16LE") 
    header = ['id', '英単語','発音記号', '意味']

    csv_data = CSV.generate("", headers: header, write_headers: true ) do |csv|
      request.each do |requestRow|
        csv << [requestRow['key'], requestRow['word'], requestRow['pron'], requestRow['meaning']]
      end
    end

    send_data(bom + csv_data.encode("UTF-16LE"),filename: 'retailers.csv',type: 'text/csv')
  end
end