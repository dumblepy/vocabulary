Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :rubyapi do
    get 'test', to: 'api#test'
    post 'search', to: 'api#search'
    post 'downloadCSV', to: 'api#downloadCSV'
    post 'downloadCSVMac', to: 'api#downloadCSVMac'
  end

  get 'test', to: 'application#test'
end
