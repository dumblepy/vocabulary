import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Toppage from '@/components/Main/Toppage'
// material
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
Vue.use(VueMaterial)

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/hello', name: 'HelloWorld', component: HelloWorld},
    {path: '/', name: 'Toppage', component: Toppage}
  ]
})
