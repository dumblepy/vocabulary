環境構築
===
|Python|3.7|Django|2.0|Ruby|2.5.2|Rails|5.2|
|---|---|---|---|---|---|---|---|

更新
```
apt-get update
apt-get -y upgrade
```
コンパイルに必要なモジュールインストール
```
apt install -y gcc make openssl libssl-dev zlib1g-dev libbz2-dev libreadline-dev libxml2 libxml2-dev zlib1g-dev libffi-dev
```
gitインストール
```
apt install -y git
```
MariaDBインストール
```
apt install -y mariadb-server mariadb-client

CREATE USER djangomehdi@localhost IDENTIFIED BY 'aaA08607'

GRANT ALL PRIVILEGES ON `blog`.テーブル TO 'ユーザ名'@'ホスト名';
```
Nginx、uwsgiインストール
```
apt install -y nginx uwsgi
```
railsに必要なモジュールインストール
```
apt install -y sqlite3 libsqlite3-dev
```
anyenvでrbenv構築
```
git clone https://github.com/riywo/anyenv ~/.anyenv
echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> ~/.your_profile
echo 'eval "$(anyenv init -)"' >> ~/.your_profile
exec $SHELL -l
```
bashrcを更新
```
vi ~/.bashrc
```
```
if [ -d $HOME/.anyenv ]
then
    export PATH="$HOME/.anyenv/bin:$PATH"
    eval "$(anyenv init -)"
fi
```
更新を反映
```
source ~/.bashrc
```
anyenv-updateをインストール
```
mkdir -p $(anyenv root)/plugins
git clone https://github.com/znz/anyenv-update.git $(anyenv root)/plugins/anyenv-update
```
rbenvインストール
```
anyenv install rbenv
rbenv install 2.4.4
rbenv install 2.5.1
rbenv local x.x.x
```
bundleインストール
```
rbenv exec bundle install --path vendor/bundle
```
pumaの設定
```
vi /config/puma.rb
```
```
#ここから
#_homeは各自環境で設定してください。
#https://www.gakusmemo.com/?p=608#puma-2
_proj_path = "#{File.expand_path("../..", __FILE__)}"
_proj_name = File.basename(_proj_path)
_home = "var"
 
pidfile "#{_home}/run/#{_proj_name}.pid"
bind "unix://#{_home}/run/#{_proj_name}.sock"
directory _proj_path
#ここまで追記
```
rails起動コマンド
```
rails assets:precompile RAILS_ENV=production
SECRET_KEY_BASE=$(rake secret) RAILS_SERVE_STATIC_FILES=true RAILS_ENV=production puma -w 4

```
Nginxの設定
```
upstream vocabulary {
    server unix:///var/run/puma/vocabulary.sock;
}

server {
    location /products/vocabulary {
        alias   /home/www/products/vocabulary/vocabulary-front/dist;
        index  index.html;
    }

    location /vocabulary/static {
        alias   /home/www/products/vocabulary/vocabulary-front/dist/static;
    }

    location /rubyapi {
        proxy_pass  http://vocabulary;
        root  /home/www/products/vocabulary/vocabulary-back/vocabulary-back;
    }
}
```


/home/www/products/vocabulary/vocabulary-front/dist/index.html




















---
---
Nginx, Nginx Unitインストール
```
apt install -y nginx

wget -O nginx_signing.key https://nginx.org/keys/nginx_signing.key?_ga=2.58652266.1394659740.1533031763-1439172043.1533031763

apt-key add nginx_signing.key
```
ファイル編集
```
vi /etc/apt/sources.list.d/unit.list

deb https://packages.nginx.org/unit/ubuntu/ bionic unit
deb-src https://packages.nginx.org/unit/ubuntu/ bionic unit
```
インストール実行
```
apt-get update
apt-get install -y unit
apt install -y unit-python3.6 unit-ruby
service unit restart
```
設定ファイルを作る
```
vi dumblepy_unit.json
```
```
{
    "listeners": {
        "*:8001": {
            "application": "dumblepy"
        }
    },
    "applications": {
        "dumblepy": {
            "type": "python 3.6",
            "user": "nobody",
            "path": "/home/www/blog/blog-back/dumblePy",
            "module": "wsgi"
        }
    }
}

```
Nginx Unit起動コマンド
```
curl -X PUT -d @./dumblepy_unit.json --unix-socket /var/run/control.unit.sock http://202.182.102.179/
```
設定確認
```
curl --unix-socket /var/run/control.unit.sock http://202.182.102.179/
```
設定削除
```
curl -X DELETE --unix-socket /var/run/control.unit.sock 'http://202.182.102.179/'
```
デフォルト
```
The Python 3.6 module for NGINX Unit has been installed.

To check out the sample app, run these commands:

 sudo service unit restart
 sudo service unit loadconfig /usr/share/doc/unit-python3.6/examples/unit.config
 curl http://localhost:8400/

Online documentation is available at https://unit.nginx.org
------------------------------------------------------------
The Ruby module for NGINX Unit has been installed.

To check out the sample app, run these commands:

 sudo service unit restart
 sudo service unit loadconfig /usr/share/doc/unit-ruby/examples/unit.config
 curl http://localhost:8700/

Online documentation is available at https://unit.nginx.org
```
anyenvインストール
```
git clone https://github.com/riywo/anyenv ~/.anyenv
echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> ~/.your_profile
echo 'eval "$(anyenv init -)"' >> ~/.your_profile
exec $SHELL -l
```
bashrcを更新
```
vi ~/.bashrc
```
```
if [ -d $HOME/.anyenv ]
then
    export PATH="$HOME/.anyenv/bin:$PATH"
    eval "$(anyenv init -)"
fi
```
更新を反映
```
source ~/.bashrc
```

anyenv-updateをインストール
```
mkdir -p $(anyenv root)/plugins
git clone https://github.com/znz/anyenv-update.git $(anyenv root)/plugins/anyenv-update
```
pyenvインストール
```
anyenv install pyenv
exec $SHELL -l
```
python3.7をインストール
```
bash pyenv install -l > pythonV.txt
> 3.7.0

pyenv install 3.7.0
```
rbenvインストール
```
anyenv install rbenv
exec $SHELL -l
```
ruby2.5.1インストール
```
bash rbenv install -l > rubyV.txt
> 2.5.1

rbenv install 2.5.1
```